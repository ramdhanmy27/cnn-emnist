import numpy as np
from sklearn.preprocessing import LabelBinarizer


class DataGenerator:
    def __init__(self, classes):
        self.encoder = LabelBinarizer()
        self.encoder.fit(classes)

    def csv_generator(self, dataset_path, input_shape, batch_size=32, mode="train", delimiter=",", aug=None):
        f = open(dataset_path, "r")

        while True:
            images = []
            classes = []
            i = 0

            while i < batch_size:
                line = f.readline()

                # reached EOF
                if line == "":
                    # re-read from the beginning of the file
                    f.seek(0)
                    line = f.readline()

                # ensure we don't continue to fill up the batch from samples at the beginning of the file
                if mode == "eval":
                    break

                row = line.strip().split(delimiter)
                class_name = row[0]
                image = np.array(row[1:]).astype("float32").reshape(input_shape) / 255

                classes.append(class_name)
                images.append(image)

                i += 1

            # one-hot encoding
            classes = self.encoder.transform(classes)

            # apply data augmentation
            if aug is not None:
                images, classes = next(aug.flow(np.array(images), classes, batch_size=batch_size))

            yield np.array(images), np.array(classes)
