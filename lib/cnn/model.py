import os
import sys
import numpy as np

from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.callbacks import TensorBoard
from keras_preprocessing.image import ImageDataGenerator

from lib.DataGenerator import DataGenerator
from lib.loader import load_classes, count_dataset
from pprint import pprint


class Model:
    def __init__(
        self, mapping_path,
        model_path="./data/model-xemnist.h5",
        weights_path="./data/weight-emnist.hdf5",
        input_shape=(28, 28, 1),
        log_dir="./logs"
    ):
        self.classes = load_classes(mapping_path)
        self.model_path = model_path
        self.weights_path = weights_path
        self.input_shape = input_shape
        self.log_dir = log_dir

        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        self.model = self.get_model(model_path)

    def get_model(self, model_path):
        if not os.path.exists(model_path):
            print("### Building Model")

            # Define model architecture
            model = Sequential()

            model.add(Conv2D(32, (3, 3), activation='relu', input_shape=self.input_shape))
            model.add(Conv2D(32, (3, 3), activation='relu'))
            model.add(MaxPooling2D(pool_size=(2, 2)))
            model.add(Dropout(0.25))

            model.add(Flatten())
            model.add(Dense(128, activation='relu'))
            model.add(Dropout(0.5))
            model.add(Dense(len(self.classes), activation='softmax'))

            # Compile model
            model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

            return model
        else:
            print("### Model Loaded: {}".format(model_path))
            return load_model(model_path)

    def train(self, train_data_path, val_data_path, batch_size=32, epochs=10):
        classes_keys = list(self.classes.keys())
        print("### Classes ({}): {}".format(len(classes_keys), self.classes.values()))

        train_gen = DataGenerator(classes_keys)
        val_gen = DataGenerator(classes_keys)
        # aug = ImageDataGenerator(
        #     rotation_range=20,
        #     zoom_range=0.15,
        #     width_shift_range=0.2,
        #     height_shift_range=0.2,
        #     shear_range=0.15,
        #     horizontal_flip=True,
        #     fill_mode="nearest"
        # )

        # gen = train_gen.csv_generator(train_data_path, self.input_shape, batch_size=batch_size)
        # imgs, labels = list(gen)[0]
        # np.set_printoptions(threshold=sys.maxsize)
        # print(imgs.shape, labels.shape, labels)
        # quit()

        print("### Start Training")
        self.model.fit_generator(
            train_gen.csv_generator(train_data_path, self.input_shape, batch_size=batch_size),
            validation_data=val_gen.csv_generator(val_data_path, self.input_shape, batch_size=batch_size),
            callbacks=[TensorBoard(log_dir=self.log_dir, histogram_freq=0, write_graph=True, write_images=True)],
            steps_per_epoch=count_dataset(train_data_path) // batch_size,
            validation_steps=count_dataset(val_data_path) // batch_size,
            epochs=epochs,
            verbose=1,
            use_multiprocessing=True,
        )

        self.model.save(self.model_path)
        print("### Training Complete")
        print("### Model Saved: {}".format(self.model_path))

    def evaluate(self, val_data_path, batch_size=32):
        print("### Evaluating")
        classes_keys = list(self.classes.keys())

        return self.model.evaluate_generator(
            DataGenerator(classes_keys).csv_generator(val_data_path, self.input_shape, batch_size=batch_size),
            steps=count_dataset(val_data_path) // batch_size,
            use_multiprocessing=True,
            verbose=1,
        )

    def predict(self, img_test):
        return self.model.predict(img_test)
