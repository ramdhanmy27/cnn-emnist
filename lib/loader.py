import numpy as np


def load_classes(mapping_path):
    with open(mapping_path, "r") as f:
        results = {}

        for line in f:
            data = line.strip().split(" ")

            if len(data) > 0:
                results[data[0]] = data[1]

        return results


def load_csv_img(dataset_path, input_shape, delimiter=",", dtype="uint8"):
    with open(dataset_path, "r") as f:
        images = []
        classes = []

        for line in f:
            row = line.strip().split(delimiter)
            class_name = row[0]
            image = np.array(row[1:]).astype(dtype).reshape(input_shape)

            classes.append(class_name)
            images.append(image)

        return np.array(images), np.array(classes)


def count_dataset(data_path):
    with open(data_path, "r") as f:
        count = 0

        for line in f:
            if line != "":
                count += 1

        return count
