import tkinter as tk
from PIL import Image, ImageTk
import cv2

from lib.gui.status import Status
from lib.cnn.model import Model
from pprint import pprint


class Video:
    def __init__(self, parent):
        self.parent = parent
        self.model = Model("data/model.h5")

    def render(self):
        # Video Player
        self.video_player = tk.Label(self.parent)
        self.video_player.grid(row=0, column=0)

        # Status Frame
        self.status_frame = tk.LabelFrame(self.parent, text="Result")
        self.status_frame.grid(row=10, column=0, padx=10, pady=2)

        self.status = Status(self.status_frame)
        self.status.render()

    def render_frame(self):
        _, frame = self.video.read()

        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        img = Image.fromarray(cv2image)
        imgtk = ImageTk.PhotoImage(image = img)

        self.video_player.imgtk = imgtk
        self.video_player.configure(image = imgtk)
        self.video_player.after(10, self.render_frame)

        img_input = cv2.resize(frame, (28, 28));
        img_input = cv2.cvtColor(img_input, cv2.COLOR_BGR2GRAY)

        prediction = self.model.predict(img_input.reshape(1, 28, 28, 1))
        self.status.updateValue(prediction.argmax())

    def play(self, file=0):
        self.video = cv2.VideoCapture(file)
        self.render_frame()
