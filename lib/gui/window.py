import tkinter as tk
from PIL import Image, ImageTk

from lib.gui.video import Video


class Window:
    def __init__(self, title):
        self.window = tk.Tk()  # Makes main window
        self.window.wm_title(title)
        self.window.config(background="#FFFFFF")

    def render(self):
        # Video Player
        video_frame = tk.Frame(self.window, width=600, height=500)
        video_frame.grid(row=0, column=0, padx=10, pady=2)

        video = Video(video_frame)
        video.render()
        video.play("data/mnist.mp4")
        # video.play()

    def run(self):
        self.window.mainloop()
