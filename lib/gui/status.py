import tkinter as tk
from tkinter import StringVar


class Status:
    def __init__(self, parent, default_text="-"):
        self.parent = parent
        self.text = StringVar()
        self.text.set(default_text)

    def updateValue(self, text):
        self.text.set(text)

    def render(self):
        self.label = tk.Label(self.parent, textvariable=self.text, anchor="center")
        self.label.grid(row=0, column=0)
