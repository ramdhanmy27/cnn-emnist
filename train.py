from lib.cnn.model import Model
from optparse import OptionParser

# parser = OptionParser()
# parser.add_option("-t", "--train_path", dest="train_path", help="Path to training data.")
# parser.add_option("-v", "--test_path", dest="test_path", help="Path to test data.")
# parser.add_option("-m", "--mapping_path", dest="mapping_path", help="Path to Mapping Classes.")
#
# options, args = parser.parse_args()
# mapping_path = options.mapping_path
# dataset_train_path = options.train_path
# dataset_test_path = options.test_path

# EMNIST
mapping_path = "/home/ramdhan/skripsi/dataset/emnist/emnist-byclass-mapping.txt"
dataset_train_path = "/home/ramdhan/skripsi/dataset/emnist/emnist-byclass-train.csv"
dataset_test_path = "/home/ramdhan/skripsi/dataset/emnist/emnist-byclass-test.csv"

model = Model(mapping_path)
model.train(dataset_train_path, dataset_test_path)

# Evaluation
loss, accuracy = model.evaluate(dataset_test_path)
print('Test loss:', loss)
print('Test accuracy:', accuracy)
